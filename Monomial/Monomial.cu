#include <utility>
#include <map>

#include "Monomial.cuh"

// constructors for class Monomial
// --------------------------------------------------------------------------------------------------------------
__host__ Monomial::Monomial(std::string monomial_str, int count_of_vars) {
    count_of_variables = count_of_vars;
    monomial = _generateMonomial(std::move(monomial_str));
    degree = _computationDegree();
}

__host__ Monomial::Monomial(std::map<int, int> monomial_map, int count_of_vars) {
    count_of_variables = count_of_vars;
    monomial = std::move(monomial_map);
    degree = _computationDegree();
}

__host__ Monomial::Monomial() {}

// functions for generating attributes of class Monomial (degree, map-monomial)
// --------------------------------------------------------------------------------------------------------------
__host__ int Monomial::_computationDegree() {
    int deg = 0;
    for (auto & monomial_param: monomial)
        deg += monomial_param.second;
    return deg;
}

__host__ std::map<int, int> Monomial::_generateMonomial(std::string monomial_str) {
    std::map<int, int> result;
    std::string::size_type str_length = monomial_str.size();
    std::string::size_type i = 0;
    // generate map of monomial
    while(i < str_length){
        std::string variable;
        std::string degree_str;
        std::string::size_type j = i;
        // generate variable from str
        while (j < str_length && monomial_str[j] != '_')
            j++;
        j += 1;
        while ( j < str_length && monomial_str[j] != '^' && monomial_str[j] != '*' ){
            variable += monomial_str[j];
            j++;
        }
        //generate degree from str
        if ( j < str_length && monomial_str[j] == '^' ){
            j++;
            while ( j < str_length && std::isdigit(monomial_str[j]) ){
                degree_str += monomial_str[j];
                j++;
            }
        }
        if ( j < str_length && monomial_str[j] == '*' )
            j++;
        if (variable.empty())
            return result;
        int deg = 1;
        if (!degree_str.empty())
            deg = std::stoi(degree_str);
        result.insert(std::pair<int, int>(std::stoi(variable), deg));
        i = j;
    }
    return result;
}

__host__ std::ostream &operator<< (std::ostream &out, const Monomial &monomial) {
    if (monomial.monomial.empty()){
        out << "1";
        return out;
    }
    for (const auto & it : monomial.monomial)
        if (it.second > 1)
            out << "x_" << it.first << "^" << it.second;
        else
            out << "x_" << it.first;
    return out;
}

// lexicographic comparison
// ---------------------------------------------------------------------------------------------------------------
bool Monomial::operator< (const Monomial &right) const {
    // generate ordered list with degrees of variables in first monomial
    std::vector<int> vector_of_degrees_1(count_of_variables, 0);
    for (auto & monomial_param: monomial)
        vector_of_degrees_1[monomial_param.first - 1] = monomial_param.second;
    // generate ordered list with degrees of variables in second monomial
    std::vector<int> vector_of_degrees_2(right.count_of_variables, 0);
    for (auto & monomial_param: right.monomial){
        vector_of_degrees_2[monomial_param.first - 1] = monomial_param.second;
    }
    return vector_of_degrees_1 < vector_of_degrees_2;
}

// method for calculation LCM
// ---------------------------------------------------------------------------------------------------------------
__host__ Monomial Monomial::lcm(const Monomial &right) const {
    std::map<int, int> result;
    for (int num_of_variable = 1; num_of_variable <= count_of_variables; num_of_variable++){
        auto it_left = monomial.find(num_of_variable);
        auto it_right = right.monomial.find(num_of_variable);
        if (it_left != monomial.end() && it_right != right.monomial.end()){
            if (it_right->second > it_left->second)
                result[num_of_variable] = it_right->second;
            else
                result[num_of_variable] = it_left->second;
        }
        else
            if (it_left != monomial.end())
                result[num_of_variable] = it_left->second;
            else
                if (it_right != right.monomial.end())
                    result[num_of_variable] = it_right->second;
    }
    return Monomial(result, count_of_variables);
}

__host__ int Monomial::getDegree() const {
    return degree;
}

// Monomial_1 / Monomial_2
// ---------------------------------------------------------------------------------------------------------------
Monomial Monomial::operator/ (const Monomial &divider) const {
    std::map<int, int> result;
    for (auto & params: monomial){
        auto it = divider.monomial.find(params.first);
        if (it != divider.monomial.end() && params.second > it->second)
                result[params.first] = params.second - it->second;
        else if (it == divider.monomial.end())
            result[params.first] = params.second;
    }
    return Monomial(result, count_of_variables);
}

Monomial Monomial::operator* (const Monomial &right) const{
    std::map<int, int> result;
    if (right.getDegree() == 0)
        return Monomial(monomial, count_of_variables);
    for (auto & monomial_params: right.monomial){
        auto it = monomial.find(monomial_params.first);
        if (it == monomial.end())
            result.insert(std::pair<int, int>(monomial_params.first, monomial_params.second));
        else
            result.insert(std::pair<int, int>(it->first, it->second + monomial_params.second));
    }
    for (auto & monomial_params: monomial){
        auto it = right.monomial.find(monomial_params.first);
        if (it == right.monomial.end())
            result.insert(std::pair<int, int>(monomial_params.first, monomial_params.second));
    }
    return Monomial(result, count_of_variables);
}

__host__ bool Monomial::is_divided(const Monomial & m) const {
    for (auto & variable: monomial){
        auto it = m.monomial.find(variable.first);
        if (it == m.monomial.end() || it->second < variable.second)
            return false;
    }
    return true;
}

bool Monomial::operator==(const Monomial &right) const {
    return monomial == right.monomial;
}

bool Monomial::operator!=(const Monomial &right) const {
    return monomial != right.monomial;
}

bool Monomial::operator>(const Monomial &right) const {
    // generate ordered list with degrees of variables in first monomial
    std::vector<int> vector_of_degrees_1(count_of_variables, 0);
    for (auto & monomial_param: monomial)
        vector_of_degrees_1[monomial_param.first - 1] = monomial_param.second;
    // generate ordered list with degrees of variables in second monomial
    std::vector<int> vector_of_degrees_2(right.count_of_variables, 0);
    for (auto & monomial_param: right.monomial){
        vector_of_degrees_2[monomial_param.first - 1] = monomial_param.second;
    }
    return vector_of_degrees_1 > vector_of_degrees_2;
}

__host__ std::set<Monomial> Monomial::divisors() const {
    std::set<Monomial> result_set;
    for (auto it = monomial.begin(); it != monomial.end(); it++){
        auto all_monomials_for_variable = _helpfulForDivisors(it->first);
        // collect new monomials
        std::set<Monomial> new_monomials;
        for (auto & monom: result_set){
            for (auto & new_monom: all_monomials_for_variable)
                new_monomials.insert(monom * new_monom);
        }
        for (auto & monom: new_monomials){
            result_set.insert(monom);
        }
        // collect monomial with one variable
        for (auto & current_monomial: all_monomials_for_variable)
            result_set.insert(current_monomial);
    }
    return result_set;
}

__host__ std::set<Monomial> Monomial::_helpfulForDivisors(int iter_variable) const {
    std::set<Monomial> result;
    int variable_degree = monomial.at(iter_variable);
    for (int i = 1; i <=variable_degree; i++){
        std::string monomial = "x_";
        if (i == 1)
            monomial += std::to_string(iter_variable);
        else
            monomial += std::to_string(iter_variable) + "^" + std::to_string(i);
        result.insert(Monomial(monomial, count_of_variables));
    }
    return result;
}

__host__ int Monomial::degree_of_variable(int variable) const {
    return monomial.count(variable) ? monomial.at(variable) : 0;
}

__host__ Monomial Monomial::divide_variables(std::vector<int> divide_vector) const {
    std::map<int, int> new_monomial;
    for (auto i = 0 ; i < divide_vector.size(); i++){
        if (monomial.count(i + 1) > 0 && divide_vector[i] != -1){
            if (monomial.at(i + 1) - divide_vector[i] != 0)
                new_monomial[i + 1] = monomial.at(i + 1) - divide_vector[i];
        }
    }
    return Monomial(new_monomial, count_of_variables);
}










