#include <string>
#include <utility>
#include <iostream>

#include "Polynomial.cuh"

// constructors for generate Polynomial from string
// -----------------------------------------------------------------------------------------------------------
Polynomial::Polynomial(std::string poly_str, int count_of_vars) {
    count_of_variables = count_of_vars;
    _generatePolynomial(std::move(poly_str));
    HT = poly.rbegin()->first;
}

__host__ Polynomial::Polynomial(std::map<Monomial, int> poly_map, int count_of_vars) {
    poly = poly_map;
    count_of_variables = count_of_vars;
    HT = poly.rbegin()->first;
}

__host__ Polynomial::Polynomial() {}


__host__ void Polynomial::_generatePolynomial(std::string poly_str) {
    std::string::size_type poly_length = poly_str.size();
    std::string::size_type i = 0;
    while (i < poly_length){
        int j = i;
        int coefficient = 1;
        std::string monomial_str;

        if (j < poly_length && poly_str[j] == '-'){
            coefficient = -1;
            j++;
        }
        if (j < poly_length && poly_str[j] == '+'){
            coefficient = 1;
            j++;
        }
        while (j < poly_length && poly_str[j] != '-' && poly_str[j] != '+'){
            monomial_str += poly_str[j];
            j++;
        }
        i = j;
        poly.insert(std::pair<Monomial, int>(Monomial(monomial_str, count_of_variables), coefficient));
    }
}

Monomial Polynomial::getHT() const {
    return HT;
}

__host__ bool Polynomial::operator< (const Polynomial &right) const {
    auto it_right_poly = right.poly.rbegin();
    for (auto it = poly.rbegin(); it != poly.rend(); it++){
        if (it_right_poly == right.poly.rend())
            return false;
        else {
            if (it->first < it_right_poly->first)
                return true;
            if (it->first > it_right_poly->first)
                return false;
        }
        it_right_poly ++;
    }
    return !(it_right_poly == right.poly.rend());
}

__host__ Polynomial Polynomial::operator* (const Monomial & m) const{
    std::map<Monomial, int> result;
    for (auto & params: poly)
        result.insert(std::pair<Monomial, int>(params.first * m, params.second));
    return Polynomial(result, count_of_variables);
}

__host__ std::map<Monomial, int> Polynomial::getAllMonomials() const {
    return poly;
}

__host__ bool Polynomial::operator!=(const Polynomial &right) const {
    return poly != right.poly;
}

__host__ std::ostream &operator<<(std::ostream &out, const Polynomial &polynomial) {
    for ( auto it = polynomial.poly.rbegin(); it != polynomial.poly.rend(); it++ ){
        if (it->second == 1 && it != polynomial.poly.rbegin())
            out << "+" << it->first;
        else if (it->second == -1)
            out << "-" << it->first;
        else
            out << it->first;
    }
    return out;
}

__host__ Polynomial Polynomial::divide() const {
    std::vector<int> variable_degree(count_of_variables, -1);
    std::map<Monomial, int> new_poly;
    for (auto i = 0; i < count_of_variables; i++) {
        for (auto &monomial: poly) {
            auto cur_degree = monomial.first.degree_of_variable(i + 1);
            if (variable_degree[i] == -1 || variable_degree[i] > cur_degree)
                variable_degree[i] = cur_degree;
        }
    }
    for (auto &monomial: poly)
        new_poly.insert({monomial.first.divide_variables(variable_degree), monomial.second});
    return Polynomial(new_poly , count_of_variables);
}



