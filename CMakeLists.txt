cmake_minimum_required(VERSION 3.17)
project(diploma CUDA)

set(CMAKE_CUDA_STANDARD 14)

add_executable(diploma main.cu Polynomial/Polynomial.cu Polynomial/Polynomial.cuh Monomial/Monomial.cu Monomial/Monomial.cuh)

set_target_properties(
        diploma
        PROPERTIES
        CUDA_SEPARABLE_COMPILATION ON)